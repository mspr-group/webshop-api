package fr.epsi.webshopapi.Repository;

import fr.epsi.webshopapi.Model.Prospect;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ProspectRepository extends JpaRepository<Prospect, Long> {
    
}
