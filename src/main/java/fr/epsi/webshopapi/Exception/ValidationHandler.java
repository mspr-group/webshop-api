package fr.epsi.webshopapi.Exception;

import java.util.Date;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ValidationHandler {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDetails> customerValidationErrorHandling(MethodArgumentNotValidException exception) {
        if (exception.getBindingResult().getFieldError() == null) {
            throw new NullPointerException();
        }
        if (exception.getBindingResult().getFieldError().getDefaultMessage() == null) {
            throw new NullPointerException();
        }
        ErrorDetails errorDetails = new ErrorDetails(new Date(), "Validation Error", exception.getBindingResult().getFieldError().getDefaultMessage());
        return ResponseEntity.badRequest().body(errorDetails);
    }
}
