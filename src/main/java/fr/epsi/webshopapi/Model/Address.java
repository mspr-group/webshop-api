package fr.epsi.webshopapi.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "Address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank
    @Size(min = 1, max = 7, message = "Number must be between 10 and 14 characters")
    private String number;
    @NotBlank
    @Size(min = 6, max = 30, message = "Street must be between 6 and 30 characters")
    private String street;
    @NotBlank
    @Size(min = 3, max = 20, message = "City must be between 3 and 20 characters")
    private String city;
    @NotBlank
    @Size(min = 5, max = 5, message = "Postal code must be between 6 and 6 characters")
    private String postalCode;

    public Address(String number, String street, String city, String postalCode) {
        this.number = number;
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
    }
}