package fr.epsi.webshopapi.Model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Customer")
public class Customer extends Person{
    public Customer() {
        super();
    }

    public Customer(Long id, String firstName, String lastName, Address address, String phoneNumber) {
        super(id, firstName, lastName, address, phoneNumber);
    }

    public Customer(String firstName, String lastName, Address address, String phoneNumber) {
        super(firstName, lastName, address, phoneNumber);
    }
}
