package fr.epsi.webshopapi.IntegrationTests;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import fr.epsi.webshopapi.Model.Address;
import fr.epsi.webshopapi.Model.Prospect;
import fr.epsi.webshopapi.Repository.ProspectRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;

@AutoConfigureMockMvc(addFilters = false)
@Testcontainers
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@SpringBootTest
class ProspectControllerTest {

    @Autowired
    private MockMvc mvc;
 
    @Autowired
    private ProspectRepository prospectRepository;

    @Container
    public static PostgreSQLContainer<?> container = new PostgreSQLContainer<>("postgres:11.1")
        .withDatabaseName("webshop")
        .withUsername("postgres")
        .withPassword("pass");

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.password", container::getPassword);
        registry.add("spring.datasource.username", container::getUsername);
    }

    @BeforeAll
    static void initAll() {
        container.start();
    }

    @Test
    @Order(1)
    void testAddProspect() throws Exception {
        Prospect prospect = new Prospect();
        prospect.setFirstName("Youness");
        prospect.setLastName("ELGHAZI");
        prospect.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
        prospect.setPhoneNumber("0606060606");

        prospectRepository.save(prospect);

        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(prospect);

        mvc.perform(post("/webshop/prospects")
            .contentType(MediaType.APPLICATION_JSON)
            .content(requestJson))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()));
    }

    @Test
    @Order(2)
    void testGetAllProspects() throws Exception {
        mvc.perform(get("/webshop/prospects")
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
            .andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].firstName", is("Youness")));
    }

    @Test
    @Order(3)
    void testGetProspectById() throws Exception {
        Prospect prospect = new Prospect();
        prospect.setFirstName("Youcef");
        prospect.setLastName("MERZOUGUI");
        prospect.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
        prospect.setPhoneNumber("0606060505");

        Long prospectId = prospectRepository.save(prospect).getId();

        mvc.perform(get("/webshop/prospects/"+prospectId)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
			.andExpect(jsonPath("$.firstName", is("Youcef")));
    }

    @Test
    @Order(4)
    void testUpdateProspect() throws Exception {
        Prospect prospect = new Prospect();
        prospect.setFirstName("Ismail");
        prospect.setLastName("Rekis");
        prospect.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
        prospect.setPhoneNumber("0753189114");

        Long prospectId = prospectRepository.save(prospect).getId();

        prospect.setId(prospectId);
        prospect.setAddress(new Address("5bis", "Boulevard de Berlin", "Nantes", "44000"));
        
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        String requestJson=ow.writeValueAsString(prospect);

        mvc.perform(put("/webshop/prospects")
            .contentType(MediaType.APPLICATION_JSON)
            .content(requestJson))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
			.andExpect(jsonPath("$.address.street", is("Boulevard de Berlin")))
            .andExpect(jsonPath("$.address.city", is("Nantes")))
            .andExpect(jsonPath("$.address.postalCode", is("44000")));
    }

    @Test
    @Order(5)
    void testDeleteProspect() throws Exception {
        Prospect prospect = new Prospect();
        prospect.setFirstName("Ryan");
        prospect.setLastName("LE BRUN");
        prospect.setAddress(new Address("5bis", "Rue René Prolongée", "Marseille", "44000"));
        prospect.setPhoneNumber("0753189114");

        Long prospectId = prospectRepository.save(prospect).getId();

        mvc.perform(delete("/webshop/prospects/"+prospectId)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
    }
}
