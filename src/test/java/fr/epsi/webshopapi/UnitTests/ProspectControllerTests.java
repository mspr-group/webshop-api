package fr.epsi.webshopapi.UnitTests;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.hamcrest.Matchers.*;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import fr.epsi.webshopapi.Controller.ProspectController;
import fr.epsi.webshopapi.Model.Address;
import fr.epsi.webshopapi.Model.Prospect;
import fr.epsi.webshopapi.Service.ProspectService;

@WebMvcTest(controllers = ProspectController.class)
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc(addFilters = false)
class ProspectControllerTests {
    @Autowired
    private MockMvc mockMvc;

	@Autowired
    ObjectMapper mapper;

    @MockBean
    private ProspectService prospectService;
	
	Prospect PROSPECT_1 = new Prospect(1L, "Youness", "ELGHAZI", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0615462157");
	Prospect PROSPECT_2 = new Prospect(2L, "Youcef", "MERZOUGUI", new Address("10", "Rue de Berlin", "Lyon", "69003"), "0516485120");
	Prospect PROSPECT_3 = new Prospect(3L, "Ismail", "REKIS", new Address("22ter", "Rue René Prolongée", "Lyon", "69003"), "0617423958");
	Prospect PROSPECT_4 = new Prospect(4L, "Ryan", "LE BRUN", new Address("2", "Rue René Prolongée", "Lyon", "69003"), "0617495231");

    @Test
    void testGetProspects() throws Exception {
		List<Prospect> prospectsList = new ArrayList<>(Arrays.asList(PROSPECT_1, PROSPECT_2, PROSPECT_3, PROSPECT_4));
		Mockito.when(prospectService.getProspects()).thenReturn(prospectsList);
        mockMvc.perform(MockMvcRequestBuilders
			.get("/webshop/prospects")
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(4)))
			.andExpect(jsonPath("$[2].firstName", is("Ismail")))
			.andDo(MockMvcResultHandlers.print());
    }

	@Test
    void testGetProspectById() throws Exception {
		Mockito.when(prospectService.getProspectById(PROSPECT_1.getId())).thenReturn(PROSPECT_1);
        mockMvc.perform(MockMvcRequestBuilders
			.get("/webshop/prospects/"+1L)
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", notNullValue()))
			.andExpect(jsonPath("$.firstName", is("Youness")));
    }

	@Test
	void testAddProspect() throws Exception {

    	Mockito.when(prospectService.addProspect(PROSPECT_1)).thenReturn(PROSPECT_1);
		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.post("/webshop/prospects")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(PROSPECT_1));

		mockMvc.perform(mockRequest)
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
            .andExpect(jsonPath("$.firstName", is("Youness")));
	}

	@Test
	void testUpdateProspect() throws Exception {
		Prospect updatedRecord = new Prospect();
		updatedRecord.setId(1L);
		updatedRecord.setFirstName("Ahmed");
		updatedRecord.setLastName("ELGHAZI");
		updatedRecord.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
		updatedRecord.setPhoneNumber("0751189114");

    	Mockito.when(prospectService.updateProspect(updatedRecord)).thenReturn(updatedRecord);

		MockHttpServletRequestBuilder mockRequest = MockMvcRequestBuilders.put("/webshop/prospects")
            .contentType(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .content(this.mapper.writeValueAsString(updatedRecord));

		mockMvc.perform(mockRequest)
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", notNullValue()))
            .andExpect(jsonPath("$.firstName", is("Ahmed")))
			.andExpect(jsonPath("$.phoneNumber", is("0751189114")));
	}

	@Test
	void testDeleteProspect() throws Exception {
    	Mockito.when(prospectService.getProspectById(2L)).thenReturn(PROSPECT_2);

    	mockMvc.perform(MockMvcRequestBuilders
            .delete("/webshop/prospects/"+2L)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk());
	}
}
