package fr.epsi.webshopapi.UnitTests;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;

import fr.epsi.webshopapi.Model.Address;
import fr.epsi.webshopapi.Model.Customer;
import fr.epsi.webshopapi.Repository.CustomerRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CustomerRepositoryTests {
    @Autowired
	private CustomerRepository customerRepository;

    @Container
    public static PostgreSQLContainer<?> container = new PostgreSQLContainer<>("postgres:11.1")
        .withDatabaseName("webshop")
        .withUsername("postgres")
        .withPassword("pass");

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.password", container::getPassword);
        registry.add("spring.datasource.username", container::getUsername);
    }

    @BeforeAll
    static void initAll() {
        container.start();
    }

    @AfterEach
    void cleanUp() {
        this.customerRepository.deleteAll();
        CUSTOMER_1 = new Customer("Youness", "ELGHAZI", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0615462157");
    }

    Customer CUSTOMER_1 = new Customer("Youness", "ELGHAZI", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0615462157");

    @Test
    public void testSaveCustomer() throws Exception {
        Customer result = customerRepository.save(CUSTOMER_1);

        assertNotNull(result);
        assertEquals("Youness", result.getFirstName());
    }

    @Test
    public void testSaveCustomerNull() throws Exception {

        InvalidDataAccessApiUsageException thrown = assertThrows(
            InvalidDataAccessApiUsageException.class,
           () -> customerRepository.save(null)
        );

        assertEquals(thrown.getClass(), InvalidDataAccessApiUsageException.class);
    }

    @Test
    public void testFindCustomerById() throws Exception {
        
        Long customerId = customerRepository.save(CUSTOMER_1).getId();
        
        Customer result = customerRepository.findById(customerId).orElse(null);

        assertNotNull(result);
        assertEquals(customerId, result.getId());
        assertEquals("Youness", result.getFirstName());
    }

    @Test
    public void testFindCustomerByIdNotFound() throws Exception {
        
        Customer result = customerRepository.findById(1L).orElse(null);

        assertNull(result);
    }

    @Test
    public void testFindCustomerByIdWithNullId() throws Exception {
        
        InvalidDataAccessApiUsageException thrown = assertThrows(
            InvalidDataAccessApiUsageException.class,
           () -> customerRepository.findById(null)
        );

        assertEquals(thrown.getClass(), InvalidDataAccessApiUsageException.class);
    }

    @Test
    public void testFindAllCustomers() throws Exception {
        customerRepository.save(CUSTOMER_1);

        List<Customer> result = customerRepository.findAll();

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Youness", result.get(0).getFirstName());
    }

    @Test
    public void testUpdateCustomer() throws Exception {

        Long customerId = customerRepository.save(CUSTOMER_1).getId();

        CUSTOMER_1.setId(customerId);
        CUSTOMER_1.setAddress(new Address("5bis", "Boulevard de Berlin", "Nantes", "44000"));
        
        Customer result = customerRepository.save(CUSTOMER_1);

        assertNotNull(result);
        assertEquals(customerId, result.getId());
        assertEquals("Boulevard de Berlin", result.getAddress().getStreet());
        assertEquals("Nantes", result.getAddress().getCity());
        assertEquals("44000", result.getAddress().getPostalCode());
    }

    @Test
    public void testUpdateCustomerNotFound() throws Exception {

        Customer updatedCustomer = new Customer();
        updatedCustomer.setId(0L);
        updatedCustomer.setFirstName("Ismail");
        updatedCustomer.setLastName("Rekis");
        updatedCustomer.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
        updatedCustomer.setPhoneNumber("0753189114");
        
        Customer result = customerRepository.save(updatedCustomer);

        assertNotNull(result);
        assertEquals(1, customerRepository.findAll().size());
    }

    @Test
    public void testDeleteCustomer() throws Exception {
        

        Long customerId = customerRepository.save(CUSTOMER_1).getId();
        
        customerRepository.deleteById(customerId);
        Customer result = customerRepository.findById(customerId).orElse(null);

        assertNull(result);
        assertEquals(0, customerRepository.findAll().size());
    }

    @Test
    public void testDeleteCustomerNotFound() throws Exception {

        EmptyResultDataAccessException thrown = assertThrows(
            EmptyResultDataAccessException.class,
           () -> customerRepository.deleteById(0L)
        );

        assertEquals(thrown.getClass(), EmptyResultDataAccessException.class);
    }

    @Test
    public void testDeleteCustomerWithNullId() throws Exception {
        
        InvalidDataAccessApiUsageException thrown = assertThrows(
            InvalidDataAccessApiUsageException.class,
           () -> customerRepository.findById(null)
        );

        assertEquals(thrown.getClass(), InvalidDataAccessApiUsageException.class);
    }
}
