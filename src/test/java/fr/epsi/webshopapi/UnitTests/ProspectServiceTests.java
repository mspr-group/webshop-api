package fr.epsi.webshopapi.UnitTests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import fr.epsi.webshopapi.Exception.InvalidRequestException;
import fr.epsi.webshopapi.Exception.NotFoundException;
import fr.epsi.webshopapi.Model.Address;
import fr.epsi.webshopapi.Model.Prospect;
import fr.epsi.webshopapi.Repository.ProspectRepository;
import fr.epsi.webshopapi.Service.ProspectService;

@ExtendWith(MockitoExtension.class)
class ProspectServiceTests {
    @InjectMocks
	ProspectService prospectService;

	@Mock
	ProspectRepository prospectRepository;

    Prospect PROSPECT_1 = new Prospect(1L, "Youness", "ELGHAZI", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0615462157");
	Prospect PROSPECT_2 = new Prospect(2L, "Youcef", "MERZOUGUI", new Address("10", "Rue de Berlin", "Lyon", "69003"), "0516485120");

    @Test
	public void testGetProspects()
	{
		List<Prospect> prospectList = new ArrayList<>(Arrays.asList(PROSPECT_1, PROSPECT_2));

		Mockito.when(prospectRepository.findAll()).thenReturn(prospectList);

		//test
		List<Prospect> result = prospectService.getProspects();

		assertEquals(2, result.size());
		verify(prospectRepository, times(1)).findAll();
	}

    @Test
	public void testGetProspectById()
	{
		Mockito.when(prospectRepository.findById(2L)).thenReturn(Optional.of(PROSPECT_2));

		//test
		Prospect result = prospectService.getProspectById(2L);

		assertEquals(2L, result.getId());
		assertEquals("Youcef", result.getFirstName());
		assertEquals("MERZOUGUI", result.getLastName());
        assertEquals("Rue de Berlin", result.getAddress().getStreet());
        assertEquals("0516485120", result.getPhoneNumber());
	}

    @Test
    public void testGetProspectByIdNotFoundException() throws Exception {

        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> prospectService.getProspectById(5L),
           "Prospect with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Prospect with id 5 not found!"));
    }

    @Test
	public void testAddProspect() {

		Mockito.when(prospectRepository.save(PROSPECT_1)).thenReturn(PROSPECT_1);

		//test
		Prospect result = prospectService.addProspect(PROSPECT_1);

		assertEquals("Youness", result.getFirstName());
		assertEquals("ELGHAZI", result.getLastName());
        assertEquals("5bis", result.getAddress().getNumber());
        assertEquals("Rue René Prolongée", result.getAddress().getStreet());
        assertEquals("Lyon", result.getAddress().getCity());
        assertEquals("69003", result.getAddress().getPostalCode());
        assertEquals("0615462157", result.getPhoneNumber());
	}

    @Test
	public void testUpdateProspect() {
        Prospect prospect = new Prospect(1L, "Ahmed", "DIAGNE", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0618923456");
        
        Mockito.when(prospectRepository.findById(1L)).thenReturn(Optional.ofNullable(prospect));
		Mockito.when(prospectRepository.save(prospect)).thenReturn(prospect);

		//test
		Prospect result = prospectService.updateProspect(prospect);

		assertEquals("Ahmed", result.getFirstName());
		assertEquals("DIAGNE", result.getLastName());
        assertEquals("5bis", result.getAddress().getNumber());
        assertEquals("Rue René Prolongée", result.getAddress().getStreet());
        assertEquals("Lyon", result.getAddress().getCity());
        assertEquals("69003", result.getAddress().getPostalCode());
        assertEquals("0618923456", result.getPhoneNumber());
	}

    @Test
    public void testUpdateProspectNotFoundException() throws Exception {
        Prospect prospect = new Prospect(5L, "Ahmed", "DIAGNE", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0618923456");

        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> prospectService.updateProspect(prospect),
           "Prospect with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Prospect with id 5 not found!"));
    }

    @Test
    public void testUpdateProspectWithNullId() throws Exception {
        Prospect prospect = new Prospect(null, "Ahmed", "DIAGNE", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0618923456");

        InvalidRequestException thrown = assertThrows(
            InvalidRequestException.class,
           () -> prospectService.updateProspect(prospect),
           "Prospect or id must not be null!"
        );

        assertTrue(thrown.getMessage().contentEquals("Prospect or id must not be null!"));
    }

    @Test
    public void testUpdateProspectWithNullProspect() throws Exception {
        InvalidRequestException thrown = assertThrows(
            InvalidRequestException.class,
           () -> prospectService.updateProspect(null),
           "Prospect or id must not be null!"
        );

        assertTrue(thrown.getMessage().contentEquals("Prospect or id must not be null!"));
    }

    @Test
    public void testDeleteProspect() throws Exception {
        Mockito.when(prospectRepository.findById(1L)).thenReturn(Optional.of(PROSPECT_1));

        prospectService.deleteProspect(1L);

        // verify the mocks
        verify(prospectRepository, times(1)).deleteById(1L);

    }

    @Test
    public void testDeleteCustomerNotFound() throws Exception {
        Mockito.when(prospectRepository.findById(5L)).thenThrow(new NotFoundException("Prospect with id 5 not found!"));
        NotFoundException thrown = assertThrows(
            NotFoundException.class,
           () -> prospectService.deleteProspect(5L),
           "Prospect with id 5 not found!"
        );

        assertTrue(thrown.getMessage().contentEquals("Prospect with id 5 not found!"));
    }
}
