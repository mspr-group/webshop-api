package fr.epsi.webshopapi.UnitTests;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;

import fr.epsi.webshopapi.Model.Address;
import fr.epsi.webshopapi.Model.Prospect;
import fr.epsi.webshopapi.Repository.ProspectRepository;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class ProspectRepositoryTests {
    @Autowired
	private ProspectRepository prospectRepository;

    @Container
    public static PostgreSQLContainer<?> container = new PostgreSQLContainer<>("postgres:11.1")
        .withDatabaseName("webshop")
        .withUsername("postgres")
        .withPassword("pass");

    @DynamicPropertySource
    static void properties(DynamicPropertyRegistry registry) {
        registry.add("spring.datasource.url", container::getJdbcUrl);
        registry.add("spring.datasource.password", container::getPassword);
        registry.add("spring.datasource.username", container::getUsername);
    }

    @BeforeAll
    static void initAll() {
        container.start();
    }

    @AfterEach
    void cleanUp() {
        this.prospectRepository.deleteAll();
        PROSPECT_1 = new Prospect("Youness", "ELGHAZI", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0615462157");
    }

    Prospect PROSPECT_1 = new Prospect("Youness", "ELGHAZI", new Address("5bis", "Rue René Prolongée", "Lyon", "69003"), "0615462157");

    @Test
    public void testSaveProspect() throws Exception {
        Prospect result = prospectRepository.save(PROSPECT_1);

        assertNotNull(result);
        assertEquals("Youness", result.getFirstName());
    }

    @Test
    public void testSaveProspectNull() throws Exception {

        InvalidDataAccessApiUsageException thrown = assertThrows(
            InvalidDataAccessApiUsageException.class,
           () -> prospectRepository.save(null)
        );

        assertEquals(thrown.getClass(), InvalidDataAccessApiUsageException.class);
    }

    @Test
    public void testFindProspectById() throws Exception {
        
        Long prospectId = prospectRepository.save(PROSPECT_1).getId();
        
        Prospect result = prospectRepository.findById(prospectId).orElse(null);

        assertNotNull(result);
        assertEquals(prospectId, result.getId());
        assertEquals("Youness", result.getFirstName());
    }

    @Test
    public void testFindProspectByIdNotFound() throws Exception {
        
        Prospect result = prospectRepository.findById(1L).orElse(null);

        assertNull(result);
    }

    @Test
    public void testFindProspectByIdWithNullId() throws Exception {
        
        InvalidDataAccessApiUsageException thrown = assertThrows(
            InvalidDataAccessApiUsageException.class,
           () -> prospectRepository.findById(null)
        );

        assertEquals(thrown.getClass(), InvalidDataAccessApiUsageException.class);
    }

    @Test
    public void testFindAllProspects() throws Exception {
        prospectRepository.save(PROSPECT_1);

        List<Prospect> result = prospectRepository.findAll();

        assertNotNull(result);
        assertEquals(1, result.size());
        assertEquals("Youness", result.get(0).getFirstName());
    }

    @Test
    public void testUpdateProspect() throws Exception {

        Long prospectId = prospectRepository.save(PROSPECT_1).getId();

        PROSPECT_1.setId(prospectId);
        PROSPECT_1.setAddress(new Address("5bis", "Boulevard de Berlin", "Nantes", "44000"));
        
        Prospect result = prospectRepository.save(PROSPECT_1);

        assertNotNull(result);
        assertEquals(prospectId, result.getId());
        assertEquals("Boulevard de Berlin", result.getAddress().getStreet());
        assertEquals("Nantes", result.getAddress().getCity());
        assertEquals("44000", result.getAddress().getPostalCode());
    }

    @Test
    public void testUpdateProspectNotFound() throws Exception {

        Prospect updatedProspect = new Prospect();
        updatedProspect.setId(0L);
        updatedProspect.setFirstName("Ismail");
        updatedProspect.setLastName("Rekis");
        updatedProspect.setAddress(new Address("5bis", "Rue René Prolongée", "Lyon", "69003"));
        updatedProspect.setPhoneNumber("0753189114");
        
        Prospect result = prospectRepository.save(updatedProspect);

        assertNotNull(result);
        assertEquals(1, prospectRepository.findAll().size());
    }

    @Test
    public void testDeleteProspect() throws Exception {
        

        Long prospectId = prospectRepository.save(PROSPECT_1).getId();
        
        prospectRepository.deleteById(prospectId);
        Prospect result = prospectRepository.findById(prospectId).orElse(null);

        assertNull(result);
        assertEquals(0, prospectRepository.findAll().size());
    }

    @Test
    public void testDeleteProspectNotFound() throws Exception {

        EmptyResultDataAccessException thrown = assertThrows(
            EmptyResultDataAccessException.class,
           () -> prospectRepository.deleteById(0L)
        );

        assertEquals(thrown.getClass(), EmptyResultDataAccessException.class);
    }

    @Test
    public void testDeleteProspectWithNullId() throws Exception {
        
        InvalidDataAccessApiUsageException thrown = assertThrows(
            InvalidDataAccessApiUsageException.class,
           () -> prospectRepository.findById(null)
        );

        assertEquals(thrown.getClass(), InvalidDataAccessApiUsageException.class);
    }
}
